﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private LevelData levelData;
    public static PlayerController Instance { get; private set; }
    private ActionController currentActionPlayer;
    private int indexNextActionSublevel;
    private Animator animator;
    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        Instance = this;
        enabled = false;
    }
    void Update()
    {
        if (currentActionPlayer == null)
        {
            if (indexNextActionSublevel == levelData.Sublevels.Length)
            {
                enabled = false;
                GameManager.Instance.LevelCompleted();
                return;
            }
            currentActionPlayer = ActionController.SetActionData(gameObject, levelData.Sublevels[indexNextActionSublevel]);
            indexNextActionSublevel++;
        }
    }
    public void Die()
    {
        animator.SetTrigger("Die");
        //var allCollider = GetComponentsInChildren<Collider>();
        //for (int i = 0; i < allCollider.Length; i++)
        //{
        //    allCollider[i].isTrigger = false;
        //}
        //var allRigidbody = GetComponentsInChildren<Rigidbody>();
        //for (int i = 0; i < allRigidbody.Length; i++)
        //{
        //    allRigidbody[i].isKinematic = false;
        //}

        currentActionPlayer.Deactivate();
        GameManager.Instance.LevelFail();
        enabled = false;
    }

    private void OnEnable()
    {
        animator.SetTrigger("Idle");
        indexNextActionSublevel = 0;
        currentActionPlayer = null;
    }
}


