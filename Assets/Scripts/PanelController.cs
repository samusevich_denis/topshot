﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelController : MonoBehaviour
{
    [SerializeField] Text textPoint;
    [SerializeField] GameObject panelResult;
    [SerializeField] Button button2X;
    public static PanelController Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        IronSourceScripts.RewardedVideoAdRewardedAction += PointX2;
    }
    public void Exit()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        GameManager.Instance.LevelStart();
    }

    public void Restart()
    {
        panelResult.SetActive(false);
        GameManager.Instance.LevelRestart();
        button2X.interactable = true;
    }

    public void ShowResults()
    {
        UpdateResults();
        panelResult.SetActive(true);
    }

    public void UpdateResults()
    {
        textPoint.text = GameController.Instance.pointPlayer.ToString();
    }
    public void ShowVideo()
    {
        IronSource.Agent.showRewardedVideo();
        FacebookEvents.Instance.LogShowAdvertisingEvent();
        button2X.interactable = false;
    }

    private void PointX2()
    {
        GameController.Instance.pointPlayer *= 2;
        UpdateResults();
    }

    private void OnDestroy()
    {
        IronSourceScripts.RewardedVideoAdRewardedAction -= PointX2;
    }
}
