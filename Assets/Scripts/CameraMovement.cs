﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private Vector3 OffsetPositionCamera;
    [SerializeField] private PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        OffsetPositionCamera = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        var positionPlayer = player.transform.position+OffsetPositionCamera;
        transform.position = Vector3.Lerp(transform.position, positionPlayer,0.4f);
    }
}
