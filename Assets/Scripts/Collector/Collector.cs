﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour
{
    private Vector3 startLocalPosition;
    private Quaternion startLocalRotation;
    private Collector[] childCollector;
    public bool isActiveStart = true;
    private bool isCollect;
    private float speedCollect = 0.5f;
    protected float positionOffset = 1.1f;
    private void Start()
    {
        CollectorController.Instance.collectors.Add(this);
        if (!isActiveStart)
        {
            return;
        }
        ActivateCollector();
    }
    void ActivateCollector()
    {
        startLocalPosition = transform.localPosition;
        startLocalRotation = transform.localRotation;
        childCollector = SetCollectorChild(transform);
    }

    void Update()
    {
        if (!isCollect)
        {
            return;
        }
        transform.localPosition = Vector3.Lerp(transform.localPosition, startLocalPosition, speedCollect* TimeController.speadAction * Time.deltaTime);
        transform.localRotation = Quaternion.Lerp(transform.localRotation, startLocalRotation, speedCollect* TimeController.speadAction * Time.deltaTime);
        var vectorAction = transform.localPosition - startLocalPosition;
        if (vectorAction.magnitude < speedCollect * TimeController.speadAction* Time.deltaTime * positionOffset)
        {
            transform.localPosition = startLocalPosition;
            transform.localRotation = startLocalRotation;
            CollectorController.Instance.countIsCollected++;
            isCollect = false;
        }
    }
    private Collector[] SetCollectorChild(Transform parrent)
    {
        var collectorsChild = new Collector[parrent.childCount];
        for (int i = 0; i < parrent.childCount; i++)
        {
            collectorsChild[i] = parrent.GetChild(i).gameObject.AddComponent<Collector>();
            collectorsChild[i].isActiveStart = false;
            collectorsChild[i].ActivateCollector();
        }
        return collectorsChild;
    }

    public void Collect()
    {
        var collider = GetComponent<Collider>();
        if (collider != null)
        {
            collider.isTrigger = true;
        }
        var rigidbody = GetComponent<Rigidbody>();
        if (rigidbody != null)
        {
            rigidbody.isKinematic = true;
        }
        isCollect = true;
    }
}
