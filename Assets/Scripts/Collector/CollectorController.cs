﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectorController : SingletonObject<CollectorController>
{
    public List<Collector> collectors = new List<Collector>();

    public int countIsCollected;
    public void Collect()
    {
        countIsCollected = 0;
        for (int i = 0; i < collectors.Count; i++)
        {
            collectors[i].Collect();
        }
    }

    public bool IsCollected()
    {
        if (countIsCollected == collectors.Count)
        {
            return true;
        }
        return false;
    }
}
