﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : SingletonObject<EnemyController>
{
    public List<EnemySkeleton> enemySkeleton = new List<EnemySkeleton>();
    private int countEnemy;

    public void AddEnemyTarget(EnemySkeleton enemy)
    {
        enemySkeleton.Add(enemy);
    }

    public EnemySkeleton GetEnemy()
    {
        for (int i = 0; i < enemySkeleton.Count; i++)
        {
            if (countEnemy == enemySkeleton[i].enemyID)
            {
                countEnemy++;
                return enemySkeleton[i];
            }
        }
        Debug.Log("Enemy no found");
        return null;
    }

    public void Restart(){
        for (int i = 0; i < enemySkeleton.Count; i++)
        {
            enemySkeleton[i].Restart();
        }
        countEnemy = 0;
    }

    
}
