﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySkeleton : MonoBehaviour
{
    public int enemyID;
    private AnimationBones[] bones;
    private PointShot pointShotWeapon;
    private Animator animator;
    private bool isShot;
    private bool triggerShot = true;
    private Transform target;
    private void Start()
    {
        EnemyController.Instance.AddEnemyTarget(this);
        bones = GetComponentsInChildren<AnimationBones>();
        animator = GetComponent<Animator>();
        pointShotWeapon = GetComponentInChildren<PointShot>();
    }

    public  void Attention(Transform target)
    {
        for (int i = 0; i < bones.Length; i++)
        {
            bones[i].SetTarget(target);
        }
        this.target = target;
    }
    public void ShotToPlayer()
    {
        isShot = true;
    }

    // Update is called once per frame
    protected  void Update()
    {
        if (isShot == triggerShot)
        {
            pointShotWeapon.Shot(target.position);
            triggerShot = false;
        }
    }
    public void StopEnemy()
    {
        enabled = false;
    }
    public void Headshot()
    {
        FacebookEvents.Instance.LogHeadShotEvent();
        GameController.Instance.pointPlayer += 2;
        Die();
    }
    public  void Die()
    {
        GameController.Instance.pointPlayer += 1;
        animator.SetTrigger("Die");
        for (int i = 0; i < bones.Length; i++)
        {
            bones[i].SetTarget();
        }
        var allCollider = GetComponentsInChildren<Collider>();
        for (int i = 0; i < allCollider.Length; i++)
        {
            allCollider[i].isTrigger = false;
        }
        enabled = false;
    }

    public  void Restart()
    {
        target = null;
        animator.SetTrigger("Idle");
        for (int i = 0; i < bones.Length; i++)
        {
            bones[i].SetTarget();
        }
        var allCollider = GetComponentsInChildren<Collider>();
        for (int i = 0; i < allCollider.Length; i++)
        {
            allCollider[i].isTrigger = true;
        }
        isShot = false;
        triggerShot = true;
        enabled = true;
    }
}
