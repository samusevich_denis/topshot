﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TimeController 
{
    private static float timeScaleDefault = 1;
    private static float fixedDeltaTimeDefault = 0.02f;
    public static float speadAction = 7; 

    public static void SetTimeDefault()
    {
        Time.timeScale = timeScaleDefault;
        Time.fixedDeltaTime = fixedDeltaTimeDefault;
    }


    public static void SetTimeScale(float scale)
    {
        Time.timeScale = scale;
        Time.fixedDeltaTime = fixedDeltaTimeDefault * scale;
    }

}
