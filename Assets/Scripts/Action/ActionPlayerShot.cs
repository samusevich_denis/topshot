﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionPlayerShot : ActionAdditional
{
    private PointShotPlayer pointShot;
    protected override void PerformAnAction()
    {
        if (actionCurve.Evaluate(currentPositionX) > 1 && Input.GetMouseButtonDown(0))
        {

            pointShot.Shot();
        }
    }

    public void SetData(ActionDataPlayerShot actionDataPlayerShot)
    {
        pointShot = GetComponentInChildren<PointShotPlayer>();
        base.SetDataCurve(actionDataPlayerShot.curveShot);
    }
}
