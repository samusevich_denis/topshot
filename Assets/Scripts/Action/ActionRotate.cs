﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActionRotate : ActionAdditional
{
    protected Transform objectRotate;
    protected float maxAngleRotate;

    protected override void Stop(){
        objectRotate.transform.localRotation = Quaternion.identity;
        base.Stop();
    }
    protected override void SetDataCurve(AnimationCurve curve){
        base.SetDataCurve(curve);
    }
}
