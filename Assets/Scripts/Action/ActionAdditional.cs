﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActionAdditional : ActionPlayer
{
    protected AnimationCurve actionCurve;
    protected float MaxPositionX;
    protected float currentPositionX;
    protected float startPositionX;
    protected override bool ActionCheck()
    {
        currentPositionX = transform.position.x - startPositionX;
        if (MaxPositionX - currentPositionX > 0.05)
        {
            return false;
        }
        return true;
    }

    protected override void Stop()
    {
        Destroy(this);
    }

    protected override void Update()
    {
        PerformAnAction();
        if (ActionCheck())
        {
            Stop();
        }
    }
    protected virtual void SetDataCurve(AnimationCurve curve)
    {
        actionCurve =curve;
        startPositionX = transform.position.x;
        var lastKey = actionCurve.keys[actionCurve.length - 1];
        MaxPositionX = lastKey.time;
    }
    public override void Deactivate()
    {
        Stop();
    }


}
