﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionAnimationHand : ActionAdditional
{
    private AnimationBonesPlayer[] animationBonesPlayer;
    private bool isTriggerAnimation = true;
    protected override void PerformAnAction()
    {
        if (isTriggerAnimation == (actionCurve.Evaluate(currentPositionX) > 1))
        {
            for (int i = 0; i < animationBonesPlayer.Length; i++)
            {
                animationBonesPlayer[i].SetTargetForPlayer(isTriggerAnimation);
            }
            isTriggerAnimation = false;
        }
    }

    public void SetData(ActionDataAnimationHand actionDataShot)
    {
        base.SetDataCurve(actionDataShot.curveShot);
        animationBonesPlayer = animationBonesPlayer = GetComponentsInChildren<AnimationBonesPlayer>();
    }

    protected override void Stop()
    {
        for (int i = 0; i < animationBonesPlayer.Length; i++)
        {
            animationBonesPlayer[i].SetTargetForPlayer(false);
        }
        Destroy(this);
    }
}
