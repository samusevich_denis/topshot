﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionMove : ActionTranslate
{
    private Animator animator;
    private Vector3 vectorMove;
    protected override void Start()
    {
        base.Start();
        vectorMove = targetPosition - startPosition;
        vectorMove.Normalize();
        animator = GetComponentInChildren<Animator>();
        animator.SetTrigger("Run");
    }

    protected override void PerformAnAction()
    {
        transform.Translate(vectorMove * TimeController.speadAction * Time.deltaTime, Space.World);
    }

    protected override void Stop()
    {
        transform.position = targetPosition;
        animator.SetTrigger("Idle");
        base.Deactivate();
    }

}
