﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionRayShot : ActionAdditional
{
    private LineRenderer lineRenderer;
    private PointShotPlayer pointShotPlayer;
    private float rayLength = 20;

    protected override void PerformAnAction()
    {

        if (lineRenderer.enabled == (actionCurve.Evaluate(currentPositionX) < 1))
        {
            lineRenderer.enabled = !lineRenderer.enabled;
        }
        if (lineRenderer.enabled)
        {
            
            lineRenderer.SetPosition(0, pointShotPlayer.transform.position);
            Vector3 vectorLine = lineRenderer.transform.position - pointShotPlayer.transform.position;

            lineRenderer.SetPosition(1, vectorLine*rayLength+ pointShotPlayer.transform.position);
        }
    }

    public void SetData(ActionDataRayShot actionDataShot)
    {
        base.SetDataCurve(actionDataShot.curveShot);
        lineRenderer = GetComponentInChildren<LineRenderer>();
        pointShotPlayer = GetComponentInChildren<PointShotPlayer>();

        lineRenderer.enabled = false;
    }
    public override void Deactivate()
    {
        lineRenderer.enabled = false;
        Stop();
    }
}
