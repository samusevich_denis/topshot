﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionController : MonoBehaviour
{
    private List<ActionPlayer> actionsToVerify = new List<ActionPlayer>();

    public static ActionController SetActionData(GameObject gameObject, ActionSublevel actionData)
    {
        List<ActionPlayer> actionsToVerify = new List<ActionPlayer>();
        for (int i = 0; i < actionData.actionSublevel.Length; i++)
        {
            switch (actionData.actionSublevel[i])
            {
                case ActionDataMove currentActionData:
                    {
                        var action = gameObject.AddComponent<ActionMove>();
                        action.SetData(currentActionData);
                        actionsToVerify.Add(action);
                        break;
                    }
                case ActionDataJump currentActionData:
                    {
                        var action = gameObject.AddComponent<ActionJump>();
                        action.SetData(currentActionData);
                        actionsToVerify.Add(action);
                        break;
                    }
                case ActionDataRotateAxisZ currentActionData:
                    {
                        var action = gameObject.AddComponent<ActionRotateAxisZ>();
                        action.SetData(currentActionData);
                        actionsToVerify.Add(action);
                        break;
                    }
                case ActionDataRotateAxisY currentActionData:
                    {
                        var action = gameObject.AddComponent<ActionRotateAxisY>();
                        action.SetData(currentActionData);
                        actionsToVerify.Add(action);
                        break;
                    }
                case ActionDataEnemy currentActionData:
                    {
                        var action = gameObject.AddComponent<ActionEnemy>();
                        action.SetData(currentActionData);
                        actionsToVerify.Add(action);
                        break;
                    }
                case ActionDataRayShot currentActionData:
                    {
                        var action = gameObject.AddComponent<ActionRayShot>();
                        action.SetData(currentActionData);
                        actionsToVerify.Add(action);
                        break;
                    }
                case ActionDataPlayerShot currentActionData:
                    {
                        var action = gameObject.AddComponent<ActionPlayerShot>();
                        action.SetData(currentActionData);
                        actionsToVerify.Add(action);
                        break;
                    }
                case ActionDataTime currentActionData:
                    {
                        var action = gameObject.AddComponent<ActionTime>();
                        action.SetData(currentActionData);
                        actionsToVerify.Add(action);
                        break;
                    }
                case ActionDataAnimationHand currentActionData:
                    {
                        var action = gameObject.AddComponent<ActionAnimationHand>();
                        action.SetData(currentActionData);
                        actionsToVerify.Add(action);
                        break;
                    }
                default:
                    Debug.Log("incorrect ActionData");
                    throw new System.NotImplementedException();
            }

        }
        var actionController = gameObject.AddComponent<ActionController>();
        actionController.SetActionsToVerify(actionsToVerify);
        return actionController;
    }

    private void SetActionsToVerify(List<ActionPlayer> actionsToVerify)
    {
        this.actionsToVerify = actionsToVerify;
    }

    private void Update()
    {
        if (actionsToVerify.Count == 0)
        {
            Destroy(this);
        }
        for (int i = 0; i < actionsToVerify.Count; i++)
        {
            if (actionsToVerify[i] != null)
            {
                break;
            }
            actionsToVerify.RemoveAt(i);
            continue;
        }
    }

    public void Deactivate()
    {
        for (int i = 0; i < actionsToVerify.Count; i++)
        {
            actionsToVerify[i].Deactivate();
        }
        actionsToVerify.Clear();
    }
}
