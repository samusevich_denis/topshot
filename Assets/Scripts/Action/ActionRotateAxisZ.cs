﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionRotateAxisZ : ActionRotate
{
    protected override void PerformAnAction()
    {
        objectRotate.localRotation = Quaternion.Euler(0, 0, -Mathf.Lerp(0, maxAngleRotate, actionCurve.Evaluate(currentPositionX)));
    }
    public void SetData(ActionDataRotateAxisZ actionDataRotate)
    {
        maxAngleRotate = actionDataRotate.maxAngleRotateZ;
        objectRotate = transform.GetChild(0);
        base.SetDataCurve(actionDataRotate.rotateCurveZ);
    }
}
