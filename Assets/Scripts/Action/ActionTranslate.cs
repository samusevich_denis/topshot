﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionTranslate : ActionPlayer
{
    protected Vector3 startPosition;
    protected Vector3 targetPosition;
    protected float positionOffset = 1.1f;

    protected virtual void Start()
    {
        transform.position = startPosition;
    }
    protected override void Update()
    {
        PerformAnAction();
        if (ActionCheck())
        {
            Stop();
        }
    }
    protected override bool ActionCheck()
    {
        var vectorAction = transform.position - targetPosition;
        if (vectorAction.magnitude > TimeController.speadAction * Time.deltaTime * positionOffset)
        {
            return false;
        }
        return true;
    }
    protected override void PerformAnAction()
    {
        transform.Translate(Vector3.right * TimeController.speadAction * Time.deltaTime, Space.World);
    }
    protected override void Stop()
    {
        transform.position = targetPosition;
        Destroy(this);
    }
    public virtual void SetData(ActionDataTranslate actionDataTranslate)
    {
        startPosition = actionDataTranslate.startPosition;
        targetPosition = actionDataTranslate.targetPosition;
    }

    public override void Deactivate()
    {
        Destroy(this);
    }
}
