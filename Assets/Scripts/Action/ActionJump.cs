﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionJump : ActionTranslate
{
    private Vector3 vectorStepMove;
    private AnimationCurve curveJump;
    private Vector3 currentPointCurve;
    private Vector3 nextPointCurve;
    private float step = 0.1f;
    private float currentLengthStep;
    private float currentLengthWay;

    protected override void Start()
    {
        base.Start();
        SetNewVectorMove();
    }
    protected override void PerformAnAction()
    {
        if (currentLengthStep > step)
        {
            transform.position = startPosition + nextPointCurve;
            currentLengthStep -= step;
            SetNewVectorMove();
        }
        var lastPosX = transform.position.x;
        transform.Translate(vectorStepMove * TimeController.speadAction * Time.deltaTime, Space.World);
        currentLengthStep += transform.position.x - lastPosX;
    }

    public void SetData(ActionDataJump actionDataJump)
    {
        base.SetData(actionDataJump);
        curveJump = actionDataJump.curveJump;
    }
    private void SetNewVectorMove()
    {
        currentLengthWay += step;
        currentPointCurve = nextPointCurve;
        nextPointCurve = new Vector3(currentLengthWay, curveJump.Evaluate(currentLengthWay));
        vectorStepMove = nextPointCurve - currentPointCurve;
        vectorStepMove.Normalize();
        //Debug.Log(vectorStepMove);
    }
}
