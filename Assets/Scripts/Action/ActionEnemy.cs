﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionEnemy : ActionAdditional
{
    public AnimationCurve enemyAttention;
    private EnemySkeleton[] enemySkeletons;
    private int indexEnemyAttention;
    private int indexShotEnemy;
    protected override void PerformAnAction()
    {
        if (enemyAttention.Evaluate(currentPositionX)>indexEnemyAttention)
        {
            enemySkeletons[indexEnemyAttention].Attention(transform.GetChild(0));
            indexEnemyAttention++;
        }
        if (actionCurve.Evaluate(currentPositionX)>indexShotEnemy)
        {
            enemySkeletons[indexShotEnemy].ShotToPlayer();
            indexShotEnemy++;
        }
    }

    public void SetData(ActionDataEnemy actionDataEnemy)
    {
        base.SetDataCurve(actionDataEnemy.curveShotEnemy);
        enemyAttention = actionDataEnemy.enemyAttention;
        enemySkeletons = new EnemySkeleton[actionDataEnemy.amountEnemys];
        for (int i = 0; i < enemySkeletons.Length; i++)
        {
            enemySkeletons[i] = EnemyController.Instance.GetEnemy();
        }
    }
    protected override void Stop()
    {
        for (int i = 0; i < enemySkeletons.Length; i++)
        {
            enemySkeletons[i].StopEnemy();
        }
        base.Stop();
    }
}
