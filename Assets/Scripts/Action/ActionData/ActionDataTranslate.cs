﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionDataTranslate : ActionData
{
    public Vector3 startPosition;
    public Vector3 targetPosition;
}
