﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActionSublevel", menuName = "ActionData/ActionSublevel", order = 9)]
public class ActionSublevel : ScriptableObject
{
    public ActionData[] actionSublevel;
}
