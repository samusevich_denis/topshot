﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "ActionData/LevelData", order = 10)]
public class LevelData : ScriptableObject
{
    public ActionSublevel[] Sublevels; 
    
}
