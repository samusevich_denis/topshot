﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ActionDataTime", menuName = "ActionData/ActionDataTime", order = 3)]
public class ActionDataTime : ActionData
{
    public AnimationCurve timeCurve;
}
