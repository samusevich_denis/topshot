﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ActionDataAnimationHand", menuName = "ActionData/ActionDataAnimationHand", order = 9)]
public class ActionDataAnimationHand : ActionData
{
    public AnimationCurve curveShot;
}
