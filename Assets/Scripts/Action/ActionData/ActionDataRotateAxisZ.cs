﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ActionDataRotateZ", menuName = "ActionData/ActionDataRotateZ", order = 3)]
public class ActionDataRotateAxisZ : ActionData
{
    public float maxAngleRotateZ;
    public AnimationCurve rotateCurveZ;
}
