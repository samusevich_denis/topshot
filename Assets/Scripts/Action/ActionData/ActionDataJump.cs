﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActionDataJump", menuName = "ActionData/ActionDataJump", order = 1)]
public class ActionDataJump : ActionDataTranslate
{
    public AnimationCurve curveJump;
}
