﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ActionDataEnemy", menuName = "ActionData/ActionDataEnemy", order = 4)]
public class ActionDataEnemy : ActionData
{
    public int amountEnemys;
    public AnimationCurve curveShotEnemy;
    public AnimationCurve enemyAttention;
}
