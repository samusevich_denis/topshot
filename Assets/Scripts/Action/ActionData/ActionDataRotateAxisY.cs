﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ActionDataRotateY", menuName = "ActionData/ActionDataRotateY", order = 2)]
public class ActionDataRotateAxisY : ActionData
{
    public float maxAngleRotateY;
    public AnimationCurve rotateCurveY;
}
