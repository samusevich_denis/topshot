﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ActionDataRayShot", menuName = "ActionData/ActionDataRayShot", order = 5)]
public class ActionDataRayShot : ActionData
{
    public AnimationCurve curveShot;
}
