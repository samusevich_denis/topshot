﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ActionDataPlayerShot", menuName = "ActionData/ActionDataPlayerShot", order = 6)]
public class ActionDataPlayerShot : ActionData
{
    public AnimationCurve curveShot;
}
