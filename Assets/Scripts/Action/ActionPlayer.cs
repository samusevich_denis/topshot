﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActionPlayer : MonoBehaviour
{
    protected abstract void Update();
    protected abstract bool ActionCheck();
    protected abstract void PerformAnAction();
    protected abstract void Stop();
    public abstract void Deactivate();
}