﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionTime : ActionAdditional
{
    protected override void PerformAnAction()
    {
        TimeController.SetTimeScale(actionCurve.Evaluate(currentPositionX));
    }

    protected override void Stop()
    {
        TimeController.SetTimeDefault();
        base.Stop();
    }

    public void SetData(ActionDataTime actionDataTime)
    {
        base.SetDataCurve(actionDataTime.timeCurve);
    }
}
