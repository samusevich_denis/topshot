﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionRotateAxisY : ActionRotate
{
    protected override void PerformAnAction()
    {
        objectRotate.localRotation = Quaternion.Euler(0, -Mathf.Lerp(0, maxAngleRotate, actionCurve.Evaluate(currentPositionX)), 0);
    }
    public void SetData(ActionDataRotateAxisY actionDataRotate)
    {
        maxAngleRotate = actionDataRotate.maxAngleRotateY;
        objectRotate = transform.GetChild(0).GetChild(0);
        base.SetDataCurve(actionDataRotate.rotateCurveY);
    }
}
