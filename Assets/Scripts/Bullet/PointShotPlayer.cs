﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointShotPlayer : MonoBehaviour
{
    [SerializeField] Transform targetShot;
    public void Shot()
    {
        var vectorShot = targetShot.position - transform.position;
        BulletLancher.LaunchBulletPlayer(vectorShot.normalized, transform.position);
    }
}
