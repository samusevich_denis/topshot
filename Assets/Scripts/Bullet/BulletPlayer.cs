﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : Bullet
{
    private void OnTriggerEnter(Collider other) {
        if (other !=null && other.TryGetComponent<HitBoxEnemy>(out HitBoxEnemy hitBox))
        {
            hitBox.Hit();
        }
    }
}


