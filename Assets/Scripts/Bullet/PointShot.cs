﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointShot : MonoBehaviour
{
    public void Shot(Vector3 target)
    {
        var vectorShot = target - transform.position;
        BulletLancher.LaunchBulletEnemy(vectorShot.normalized, transform.position);
    }
}
