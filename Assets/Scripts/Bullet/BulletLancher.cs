﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BulletLancher
{
    private static GameObject CreateBullet(Vector3 point)
    {
        var obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        obj.transform.position = new Vector3(point.x, point.y,0);
        obj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        var rig = obj.AddComponent<Rigidbody>();
        rig.isKinematic = true;
        var collider = obj.GetComponent<SphereCollider>();
        collider.isTrigger = true;
        return obj;
    }

    public static void LaunchBulletEnemy(Vector3 direction, Vector3 point)
    {
        var obj = CreateBullet(point);
        var bullet = obj.AddComponent<BulletEnemy>();
        bullet.Launch(direction);
    }
    public static void LaunchBulletPlayer(Vector3 direction, Vector3 point)
    {
        var obj = CreateBullet(point);
        var bullet = obj.AddComponent<BulletPlayer>();
        bullet.Launch(direction);
    }
}
