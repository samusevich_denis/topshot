﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : Bullet
{
    protected override void Update()
    {
        base.Update();
        CorrectDirection();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other != null && other.TryGetComponent<HitBoxPlayer>(out HitBoxPlayer hitBox))
        {
            hitBox.Hit();
            Destroy(gameObject);
        }
    }
    private void CorrectDirection()
    {
        direction = PlayerController.Instance.transform.GetChild(0).position - transform.position;
        direction.Normalize();
    }
}
