﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    protected Vector3 direction;
    protected float speedBullet = 13;
    private float timeLife = 5;

    protected virtual void Update()
    {
        var pos = transform.position;
        pos += direction * speedBullet *TimeController.speadAction* Time.deltaTime;
        transform.position = pos;
        timeLife -= Time.deltaTime;
        if (timeLife<0)
        {
            Destroy(gameObject);
        }
    }

    public void Launch(Vector3 direction)
    {
        this.direction = new Vector3( direction.x, direction.y, 0f);
    }
}
