﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonGameObject<GameManager>
{
    private IEnumerator StartGame()
    {
        yield return null;
        //while (!CollectorController.Instance.IsCollected())
        //{
        //    yield return null;
        //}
        EnemyController.Instance.Restart();
        PlayerController.Instance.enabled = true;
    }

    public void LevelFail()
    {
        PanelController.Instance.ShowResults();
    }
    public void LevelCompleted()
    {
        FacebookEvents.Instance.LogLevelCompletedEvent(GameController.Instance.pointPlayer);
        PanelController.Instance.ShowResults();
    }
    public void LevelStart()
    {
        PlayerController.Instance.enabled = true;
    }
    public void LevelRestart()
    {
        GameController.Instance.pointPlayer = 0;
        //CollectorController.Instance.Collect();
        StartCoroutine(StartGame());
    }

}
