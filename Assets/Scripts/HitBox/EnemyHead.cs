﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHead : HitBoxEnemy
{
    public override void Hit()
    {
        enemy.Headshot();
    }

}
