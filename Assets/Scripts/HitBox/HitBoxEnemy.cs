﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HitBoxEnemy : HitBox
{
    protected EnemySkeleton enemy;

    private void Start()
    {
        enemy = GetComponentInParent<EnemySkeleton>();
    }
}