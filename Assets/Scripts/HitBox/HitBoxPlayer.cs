﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxPlayer : HitBox
{
    public override void Hit()
    {
        PlayerController.Instance.Die();
    }
}
