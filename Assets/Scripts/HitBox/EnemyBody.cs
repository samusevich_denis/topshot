﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBody : HitBoxEnemy
{
    public override void Hit()
    {
        enemy.Die();
    }
}
