﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FacebookEvents : SingletonObject<FacebookEvents>
{
    public void LogHeadShotEvent()
    {
        FB.LogAppEvent(
            "HeadShot"
        );
    }

    public void LogLevelCompletedEvent(int point)
    {
        var parameters = new Dictionary<string, object>();
        parameters["point"] = point;
        FB.LogAppEvent("LevelCompleted", null, parameters);
    }

    public void LogShowAdvertisingEvent()
    {
        FB.LogAppEvent("ShowAdvertising");
    }
}