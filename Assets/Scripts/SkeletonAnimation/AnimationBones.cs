﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationBones : MonoBehaviour
{
    [SerializeField] private Vector3 fixAngle;
    [SerializeField] private float weight;
    [SerializeField] private AnimationBones nextBones;
    [SerializeField] private bool parrentBones;
    [SerializeField] private float speedAnimation = 5;
    [SerializeField] protected Transform target;
    [SerializeField] protected bool isTakeAim;
    protected Quaternion lastQuaternion;
    [SerializeField] private Quaternion quaternionLookTarget;

    public void SetTarget(Transform target = null)
    {
        this.target = target;
        this.isTakeAim = target!=null?true:false;
        lastQuaternion = transform.rotation;
    }

    private void LateUpdate()
    {
        if (!parrentBones|| !isTakeAim)
        {
            return;
        }
        SetLookRotation();
    }

    public void SetLookRotation()
    {
        var startRotation = transform.rotation;
        transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position);
        transform.Rotate(fixAngle);
        var quaternionLook = transform.rotation;
        var quaternionEnd = Quaternion.Lerp(startRotation, quaternionLook, weight);
        transform.rotation = Quaternion.Lerp(lastQuaternion, quaternionEnd, Time.deltaTime*speedAnimation);
        lastQuaternion = transform.rotation;
        if (nextBones != null)
        {
            nextBones.SetLookRotation();
        }
        quaternionLookTarget = transform.localRotation;
    }
}
