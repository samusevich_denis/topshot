﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationBonesPlayer : MonoBehaviour
{
    [SerializeField] private Quaternion quaternionLocalRotation;
    [SerializeField] private AnimationBonesPlayer nextBones;
    [SerializeField] private bool parrentBones;
    private float speedAnimation = 10;
    [SerializeField] private bool isTakeAim;
    private Quaternion lastQuaternion;
 

    private void LateUpdate()
    {
        if (!parrentBones)
        {
            return;
        }
        SetLookRotation();
    }

    public void SetLookRotation()
    {
        if (isTakeAim)
        {
            transform.localRotation = Quaternion.Lerp(lastQuaternion, quaternionLocalRotation, Time.deltaTime * speedAnimation);
        }
        else
        {
            transform.localRotation = Quaternion.Lerp(lastQuaternion, transform.localRotation, Time.deltaTime * speedAnimation);
        }
        lastQuaternion = transform.localRotation;
        if (nextBones != null)
        {
            nextBones.SetLookRotation();
        }
    }

    public void SetTargetForPlayer(bool isSet)
    {
            this.isTakeAim = isSet;
            lastQuaternion = isSet? transform.localRotation : lastQuaternion;
    }
}
